#!/bin/bash
apt-get update
apt-get install git python-pip -y
pip install ansible 
git clone https://denis_alligand@bitbucket.org/denis_alligand/polytech.git
mkdir /root/.ssh
cp -rp /home/vagrant/polytech/keys/* /root/.ssh
chmod 700 /root/.ssh/id_rsa
